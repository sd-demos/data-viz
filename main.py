# Collect data from DB by chunks

from dotenv import load_dotenv
import os
import pandas as pd
from sqlalchemy import create_engine
import datetime
import time
import logging
from timeit import default_timer as timer

load_dotenv()

logger = logging.getLogger('data_processing')
logger.setLevel(logging.DEBUG)

ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
logger.addHandler(ch)


def connect_db():
    PG_USER = os.getenv('PG_USER')
    PG_PASS = os.getenv('PG_PASS')
    PG_SERVER = os.getenv('PG_SERVER')
    PG_DB = os.getenv('PG_DB')

    alchemyEngine = create_engine(f"postgresql+psycopg2://{PG_USER}:{PG_PASS}@{PG_SERVER}/{PG_DB}",
                                  pool_recycle=3600)

    return alchemyEngine.connect()


def connect_archive_db():
    PG_USER = os.getenv('PG_ARCHIVE_USER')
    PG_PASS = os.getenv('PG_ARCHIVE_PASS')
    PG_SERVER = os.getenv('PG_ARCHIVE_SERVER')
    PG_DB = os.getenv('PG_ARCHIVE_DB')

    alchemyEngine = create_engine(f"postgresql+psycopg2://{PG_USER}:{PG_PASS}@{PG_SERVER}/{PG_DB}",
                                  pool_recycle=3600)

    return alchemyEngine.connect()


def collect_activity(
        conn_main: object, conn_archive: object, date_from: datetime, date_to: datetime, weekday: int, hour: int, *,
        span: int = 1,
        file_name_base: str = 'data', data_dir: str = "data") -> object:
    """

    :param file_name_base: files will be named like base-yyyy-mm-dd-hh
    :param conn_main: database connection from create_engine
    :param date_from: period start (inclusive)
    :param date_to: period end (exclusive)
    :param weekday: zero-based
    :param hour: 24h format (0 - 23)
    :param span: int, how many hours to collect (1 - 24)
    :return:
    """

    os.makedirs(data_dir, exist_ok=True)

    date_start = date_from
    t = date_start.weekday()
    delta = weekday - t
    if delta != 0:
        if delta < 0:
            delta += 7
        date_start += datetime.timedelta(days=delta)

    date_start += datetime.timedelta(hours=hour)

    while date_start < date_to:
        date_end = date_start + datetime.timedelta(hours=span, seconds=-1)
        logger.info(f"Processing interval {date_start} - {date_end} with week day {date_start.weekday()}")

        string_from = date_start.strftime('%Y-%m-%d %H:%M:%S')
        string_to = date_end.strftime('%Y-%m-%d %H:%M:%S')

        # Operations table
        sql = f'select d.id::text, d.total_sum, d.created_at, ' \
              f'from data d  ' \
              f'where ' \
              f'd.created_at between \'{string_from}\' and \'{string_to}\' ' \
              f'and d.processed = true ' \
              f'limit 2000000 '

            df = pd.read_sql(sql, conn_main, parse_dates=['created_at'])

            df["id"] = df["id"].astype(str, copy=False)

            span_suffix = ''
            if span > 1:
                span_suffix = f'_{span}h'

            df.to_parquet(f"{data_dir}/{file_name_base}_{date_start.strftime('%Y-%m-%d-%H')}{span_suffix}.parquet")

        month_str = date_start.strftime('%m')

        # Metadata table
        sql = f'select d.id::text, d.key_id::text ' \
              f'from archive.data2{month_str} d  ' \
              f'where ' \
              f'd.created_at between \'{string_from}\' and \'{string_to}\' ' \
              f'limit 2000000 '

            start = timer()
            df_meta = pd.read_sql(sql, conn_archive)
            end = timer()

            df_meta["id"] = df_meta["id"].astype(str, copy=False)

            span_suffix = ''
            if span > 1:
                span_suffix = f'_{span}h'

            df_meta.to_parquet(f"{data_dir}/meta_{date_start.strftime('%Y-%m-%d-%H')}{span_suffix}.parquet")

        time.sleep(10)

        date_start += datetime.timedelta(weeks=1)


if __name__ == '__main__':
    conn = connect_db()
    conn_arch = connect_archive_db()

    datetime.datetime.strptime('2022-02-01', "%Y-%m-%d").date()
    collect_activity(conn, conn_arch, date_from=datetime.datetime(2022, 2, 1), date_to=datetime.datetime(2022, 9, 1), weekday=1,
                     hour=0, span=24)

    conn.close()
    conn_arch.close()