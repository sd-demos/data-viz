import logging

import pandas as pd
import plotly.express as px
from dotenv import load_dotenv

load_dotenv()

logger = logging.getLogger('data_processing')
logger.setLevel(logging.DEBUG)

ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
logger.addHandler(ch)


if __name__ == '__main__':
    data_full = pd.read_parquet(f"data/data_full.parquet")
    data_full.day_part = pd.Categorical(data_full.day_part)

    basic_df = data_full[["diff", "city_en", "lat", "lon", "count"]]
    locations_count = basic_df.groupby(["diff", "city_en", "lat", "lon"]).agg({"count": "sum"})
    locations_count.reset_index(inplace=True)
    locations_count["count_clipped"] = locations_count["count"].clip(lower=500)
    locations_count.loc[locations_count['count'] < 20, 'count_clipped'] = 0
    locations_count.head(3)

    fig = px.scatter_mapbox(locations_count, lat="lat", lon="lon",
                            title="Estimated business activity",
                            labels={'diff': 'Day number relative to war start '},
                            hover_name="city_en",
                            size="count_clipped",
                            size_max=55,
                            hover_data={'count': True, 'city_en': False, 'diff': False},
                            color_discrete_sequence=["blue"],
                            animation_frame="diff",
                            center={'lat':49,'lon':31},
                            zoom=6,
                            height=1050)

    fig.update_layout(
        font_size=20
    )
    fig.update_xaxes(title_font_size=24)

    fig.update_layout(mapbox_style="open-street-map")
    fig.update_layout(margin={"r": 50, "t": 50, "l": 10, "b": 0})

    #
    # basic_df = data_full[["diff", "city_en", "lat", "lon", "count"]]
    # locations_count = basic_df.groupby(["diff", "lat", "city_en", "lon"]).agg({"count":"sum"})
    # locations_count.reset_index(inplace=True)
    # locations_count["count"] = np.power(locations_count["count"], 3./5.)
    # locations_count["count"] /= locations_count["count"].max()
    #
    # fig = px.scatter_geo(locations_count, lat="lat", lon="lon",
    #                  hover_name="city_en", size="count",
    #                  animation_frame="diff",
    #                  projection="natural earth", scope='europe')
    fig.show()